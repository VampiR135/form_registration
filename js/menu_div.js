function menu (objMenu) {
    if ( $(objMenu).css('display') == 'none' ) {
        $(".menu").animate({height: 'hide'}, 300);
        $(objMenu).animate({height: 'show'}, 500);
    } else {
        $(objMenu).animate({height: 'hide'}, 300);
    }
}